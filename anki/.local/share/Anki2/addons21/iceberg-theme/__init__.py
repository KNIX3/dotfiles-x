import os

from aqt import mw
from aqt import gui_hooks

from .config import addon_path, addonfoldername, gc, getUserOption

source_absolute = os.path.join(addon_path, "sources", "css")
web_absolute = os.path.join(addon_path, "web", "css")

regex = r"(user_files.*|web.*)"
mw.addonManager.setWebExports(__name__, regex)


def update_css():
    # on startup: combine template files with config and write into webexports folder
    change_copy = [os.path.basename(f) for f in os.listdir(source_absolute) if f.endswith(".css")]
    for f in change_copy:
        with open(os.path.join(source_absolute, f)) as FO:
            filecontent = FO.read()

        with open(os.path.join(web_absolute, f), "w") as FO:
            FO.write(filecontent)
update_css()

css_files_to_replace = [os.path.basename(f) for f in os.listdir(web_absolute) if f.endswith(".css")]

def replace_css(web_content, context): 
    for idx, filename in enumerate(web_content.css): 
        filename = filename.lstrip("css/")
        if filename in css_files_to_replace:
            web_content.css[idx] = f"/_addons/{addonfoldername}/web/css/{filename}"
            web_content.css.append(f"/_addons/{addonfoldername}/user_files/css/custom_{filename}")
gui_hooks.webview_will_set_content.append(replace_css)
