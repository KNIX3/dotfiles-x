# Enable vi mode
bindkey -v

# Enable tab completion
autoload -U compinit
compinit
zstyle ':completion:*:descriptions' format '%U%B%d%b%u'
zstyle ':completion:*:warnings' format '%BSorry, no matches for: %d%b'

# Enable autocorrection
#setopt correctall

# Customize the prompt
autoload -U promptinit
promptinit
prompt gentoo

# Set up command history
export HISTSIZE=10000
export HISTFILE="${XDG_CACHE_HOME:-$HOME/.cache}/zsh-history"
export SAVEHIST="$HISTSIZE"
# Delete duplicate commands from command history
setopt hist_ignore_all_dups

# Set up eye candy
LS="ls"
if command -v colorls > /dev/null; then LS="colorls -G"; fi
if command -v colortree > /dev/null; then alias tree="colortree -F"; fi
source ~/.config/lf/lf-icons
if [ "$TERM" = "linux" ]; then
    echo -en "\e]P0161821" #black
    echo -en "\e]P1e27878" #darkred
    echo -en "\e]P2b4be82" #darkgreen
    echo -en "\e]P3e2a478" #brown
    echo -en "\e]P484a0c6" #darkblue
    echo -en "\e]P5a093c7" #darkmagenta
    echo -en "\e]P689b8c2" #darkcyan
    echo -en "\e]P7c6c8d1" #lightgrey
    echo -en "\e]P86b7089" #darkgrey
    echo -en "\e]P9e98989" #red
    echo -en "\e]PAc0ca8e" #green
    echo -en "\e]PBe9b189" #yellow
    echo -en "\e]PC91acd1" #blue
    echo -en "\e]PDada0d3" #magenta
    echo -en "\e]PE95c4ce" #cyan
    echo -en "\e]PFd2d4de" #white
    clear #for background artifacting
fi
source /usr/share/zsh/site-functions/zsh-syntax-highlighting.zsh

# Add aliases
alias ls="$LS -FHh"
alias ll="ls -l"
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias df="df -h"
alias du="du -ch"
alias ydla="yt-dlp -f 'bestaudio[ext=m4a]'"
alias srd="screen -rd"
alias zh="less ~/.cache/zsh-history"
alias neofetch="cat ~/.local/share/zsh/fetch"
alias gis="git status"
alias gia="git add"
alias gip="git push"
alias gic="git commit -S -m"
alias gicu="git commit -m"
